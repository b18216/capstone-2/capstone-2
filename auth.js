const jwt = require('jsonwebtoken');
const secret = "ClimbHardGymAPI2022"

module.exports.createAccessToken = (account) => {
    const data = {
        id: account._id,
        email: account.email,
        isAdmin: account.isAdmin
    };
    console.log(data);
    return jwt.sign(data, secret, {})
};

module.exports.verify = (req, res, next) => {
    let token = req.headers.authorization;
    if (typeof token === "undefined") {
        return res.send({
            auth: "Failed. No Token"
        })
    } else {
        token = token.slice(7, token.lenght)
        jwt.verify(token, secret, (err, decodedToken) => {
            if (err) {
                return res.send({
                    auth: "failed",
                    message: err.message
                })
            } else {
                req.account = decodedToken
                next();
            }
        });
    }
};

// VERIFY IF USER IS ADMIN
module.exports.verifyAdmin = (req, res, next) => {
    if (req.account.isAdmin) {
        next()
    } else {
        return res.send({
            auth: "Failed",
            message: "Action not Allowed"
        });
    }
};

