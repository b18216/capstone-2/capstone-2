const express = require('express');
const router = express.Router();
const accntController = require('../controllers/accountController');
const auth = require('../auth');
const {verify, verifyAdmin} = auth;


// ACCOUNTS ROUTE

// REG NEW ACCOUNT
router.post('/', accntController.regAccount);
// GET ALL ACCNT
router.get('/', verify, verifyAdmin, accntController.getAllAccnt);
// LOG IN AN ACOUNT
router.post('/login', accntController.loginAccnt);

// Make ADMIN
router.put('/makeAdmin/:id', verify, verifyAdmin, accntController.makeAdmin);

// REMOVE ADMIN
router.put('/removeAdmin/:id', verify, verifyAdmin, accntController.removeAdmin);
// PURCHASE PRODUCT
router.post('/purchaseProduct', verify, accntController.buyProduct)
// GET PURCHASE RECORD BY ACCNT
router.get('/getPurchaseRecord', verify, accntController.getAllPurchased);

// GET ALL PURCHASE RECORD
router.get('/getAllPurchaseRecord', verify, verifyAdmin, accntController.getAllPurchased);

module.exports = router;