const express = require('express');
const router = express.Router();
const prodController = require('../controllers/prodsController');
const auth = require('../auth');
const {verify, verifyAdmin} = auth;

// REG NEW PRODUCT
router.post('/', verify, verifyAdmin, prodController.newProduct);
// VIEW ALL PRODUCTS
router.get('/' , prodController.viewAllProducts);
// FIND SINGLE PRODUCT BY ID
router.get('/:id',prodController.getOneProduct);
// FIND A PRODUCT BY ID AND UPDATE
router.put('/updateProduct/:id',verify, verifyAdmin, prodController.updateProduct)
// DEACTIVATE A PRODUCT
router.put('/deactivateProduct/:id', verify, verifyAdmin, prodController.decativateProduct);
// ACTIVATE A PRODUCT
router.put('/activateProduct/:id', verify, verifyAdmin, prodController.activateProduct);
// GET ALL ACTIVE PRODUCTS
router.get('/getAllActive/active', verify, verifyAdmin, prodController.getallActive);

router.get('/getAllInactive/Inactive', verify, verifyAdmin, prodController.getallIInactive);

// GET PRODUCT BY NAME
router.get('/getproduct/byname', prodController.findProductByName);



module.exports = router;