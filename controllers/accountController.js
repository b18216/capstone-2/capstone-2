const Account = require('../models/Acounts');
const Product = require('../models/Products')
const bcrypt = require('bcryptjs');
const auth = require('../auth');

// REGISTER AN ACCOUNT

module.exports.regAccount = (req, res) => {
    const hashedPW = bcrypt.hashSync(req.body.password, 10);

    let newAccount = new Account({
        fullName: req.body.fullName,
        nickName: req.body.nickName,
        email: req.body.email,
        password: hashedPW,
        contactNo: req.body.contactNo
    });
    newAccount.save()
        .then(newAccnt => res.send(newAccnt))
        .catch(err => res.send(err));
};
// VIEW ALL ACOUNT
module.exports.getAllAccnt = (req, res) => {
    Account.find({})
        .then(result => res.send(result))
        .catch(err => res.send(err));
};
// LOG IN AN ACCOUNT
module.exports.loginAccnt = (req, res) => {
    console.log(req.body);
    Account.findOne({
            email: req.body.email
        })
        .then(foundAccnt => {
            if (foundAccnt === null) {
                return res.send("User Not Found");
            } else {
                const checkPassword = bcrypt.compareSync(req.body.password, foundAccnt.password)
                if (checkPassword) {
                    return res.send({
                        accesstoken: auth.createAccessToken(foundAccnt)
                    })
                } else {
                    return res.send("Wrong password. try again")
                }
            }
        })
        .catch(err => res.send(err));
}

// MAKE ADMIN USER
module.exports.makeAdmin = (req, res) => {

    let updates = {
        isAdmin: true
    }
    Account.findByIdAndUpdate(req.params.id, updates, {
            new: true
        })
        .then(updatedUser => res.send(updatedUser))
        .catch(err => res.send(err));
};

// REMOVE ADMIN USER
module.exports.removeAdmin = (req, res) => {

    let updates = {
        isAdmin: false
    }
    Account.findByIdAndUpdate(req.params.id, updates, {
            new: true
        })
        .then(updatedUser => res.send(updatedUser))
        .catch(err => res.send(err));
};
// PURCHASE PRODUCTS

module.exports.buyProduct = async (req, res) => {
    if (req.account.isAdmin) {
        return res.send("Action not Allowed for Admin");
    }

    // PURCHASE ACCOUNT SIDE

    let isAccntUpdated = await
    Account.findById(req.account.id).then(accnt => {
        console.log(accnt);

        let newPurchase = {
            productId: req.body.productId
        }
        accnt.purchase.push(newPurchase)
        return accnt.save()
            .then(accnt => true)
            .catch(err => err.message)
    })
    if (isAccntUpdated !== true) {
        return res.send({
            message: isAccntUpdated
        })
    }

    let isProductUpdated = await
    Product.findById(req.body.productId).then(prdct => {
        console.log(prdct);

        let clientbuy = {
            accntId: req.account.id
        }

        prdct.accntbuyers.push(clientbuy)
        
        accnt.orders.push(prdct.price)
        return prdct.save()
            .then(prdct => true)
            .catch(err => err.message);
    })
    
    if (isProductUpdated !== true) {
        return res.send({
            message: isProductUpdated
        });
    }
    
     
    if (isAccntUpdated && isProductUpdated) {
        return res.send({
            message: `Purchase Complete, Total Purchase is `
        })
    }

}



// GET PURCHASE RECORD PER ACCOUNT
module.exports.getPurchased = (req, res) => {
    Account.findById(req.account.id)
        .then(result => res.send(result.purchase))
        .catch(err => res.send(err));
};

// GET ALL PURCHASE RECORD
module.exports.getAllPurchased = (req, res) => {
    Account.find({})
        .then(result => res.send(result.purchase))
        .catch(err => res.send(err));
};


// accnt.order.push(newPurchase.price)

//    let sum = basket.reduce((acc, val)=> acc + val);

// ${sum}
// ${accnt.order}

// basket.push(price)
// console.log(basket)