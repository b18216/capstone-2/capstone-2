const Product = require('../models/Products');
const auth = require('../auth');

// ADD NEW PRODUCT

module.exports.newProduct = (req, res) => {
    let newProd = new Product({
        name: req.body.name,
        description: req.body.description,
        price: req.body.price,
    });
    newProd.save()
        .then(newProd => res.send(newProd))
        .catch(err => res.send(err));
};
// VIEW ALL PRODUCTS
module.exports.viewAllProducts = (req, res) => {
    Product.find({})
        .then(result => res.send(result))
        .catch(err => res.send(err));
};

// GET 1 PRODUCT BY ID
module.exports.getOneProduct = (req, res) => {
    Product.findById(req.params.id)
        .then(result => res.send(result))
        .catch(err => res.send(err))
};
// FIND 1 PRODUCT BY ID AND UPDATE
module.exports.updateProduct = (req, res) => {
    let update = {
        name: req.body.name,
        description: req.body.description,
        price: req.body.price
    }
    Product.findByIdAndUpdate(req.params.id, update, {
            new: true
        })
        .then(updated => res.send(updated))
        .catch(err => res.send(err))

};
// SET PRODUCT TO INACTIVE
module.exports.decativateProduct = (req, res) => {
    let deactive = {
        isActive: false
    }

    Product.findByIdAndUpdate(req.params.id, deactive, {
            new: true
        })
        .then(result => res.send(result))
        .catch(err => res.send(err));
};
// SET PRODUCT TO ACTIVE
module.exports.activateProduct = (req, res) => {
    let activate = {
        isActive: true
    }

    Product.findByIdAndUpdate(req.params.id, activate, {
            new: true
        })
        .then(result => res.send(result))
        .catch(err => res.send(err));
};

// SELECT ALL ACTIVE PRODUCTS
module.exports.getallActive = (req, res) => {
    Product.find({
            isActive: true
        })
        .then(allactive => res.send(allactive))
        .catch(err => res.send(err));
};

module.exports.getallIInactive = (req, res) => {
    Product.find({
            isActive: false
        })
        .then(allactive => res.send(allactive))
        .catch(err => res.send(err));
};

module.exports.findProductByName = (req, res) => {
    Product.find({
            name: {
                $regex: req.body.name,
                $options: '$i'
            }
        })
        .then(result => {
            if (result.length === 0) {
                return res.send('No Item Found')
            } else {
                return res.send(result)
            }
        })
}