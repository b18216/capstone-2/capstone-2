const mongoose = require('mongoose');
const productSchema = new mongoose.Schema({

    name: {
        type: String,
        required: [true, "Enter Product Name"]
    },
    description: {
        type: String,
        required: [true, "Enter Product Description"]
    },
    price: {
        type: Number,
        required: [true, "Enter Product Price"]
    },
    isActive: {
        type: Boolean,
        default: true
    
    },
    dateAdded:{
        type:Date,
        default: new Date()
    },
    accntbuyers:[{
        accntId:{
            type:String,
            required:[true, "Account ID is Required"]
        },
        dateBought:{
            type: Date,
            default:new Date()
        }
       
    }]
});
module.exports = mongoose.model("Products", productSchema);