const mongoose = require('mongoose');
const accountSchema = new mongoose.Schema({

    fullName: {
        type: String,
        required: [true, "Please Enter Full Name"]
    },
    nickName: {
        type: String,
        required: [true, "Please Enter Nickname"]
    },
    email: {
        type: String,
        required: [true, "Please Enter Email"]
    },
    password: {
        type: String,
        required: [true, "Please EnterPassword"]
    },
    contactNo: {
        type: String,
        required: [true, " Please Enter Contact Number"]
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    purchase: [{
        productId: {
            type: String,
            required: [true, "Product ID Required"]
        },
        datePurchased: {
            type: Date
        },
        status: {
            type: String,
            default: "Purchase Complete"
        },
        price:{
            type:Array,
            default:[]
        }  
}]

});

module.exports = mongoose.model("Acounts", accountSchema);