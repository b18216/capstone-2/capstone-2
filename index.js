
// DEPENDENCIES
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');


// SERVER
const app=express();
const port = 4000;


// DB CONNECTION
mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.mvn65.mongodb.net/capstone-2?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
});
let db = mongoose.connection;
db.on('error', console.error.bind(console, "connection error"));
db.once('open', () => console.log("Succesfully connected to the DataBase"));


// MIDDLEWARE
app.use(express.json());
app.use(express.urlencoded({
    extended: true
}));
app.use(cors());

// ROUTINGS
// ACCOUNTS ROUTES
const accntRoutes = require('./routes/accountsRoute');
app.use('/accounts', accntRoutes);
// PRODUCTS ROUTES
const prodRoutes = require('./routes/prodRoutes')
app.use('/products', prodRoutes);

// PORT LISTENER
app.listen(port, ()=>console.log(`Server is Running at Port: ${port}`));